var app= angular.module('myApp',[]);
app.controller('myController',function($scope){
    $scope.products=[
        {
            'tensp':'Nguyên trang',
            'soluong':0,
            'dongia':2000,
            'isBuy':false
        },
        {
            'tensp':'1/2 trang',
            'soluong':0,
            'dongia':1500,
            'isBuy':false
        },
        {
            'tensp':'1/4 trang',
            'soluong':0,
            'dongia':1000,
            'isBuy':false
        },
        {
            'tensp':'1/8 trang',
            'soluong':0,
            'dongia':850,
            'isBuy':false
        },
        {
            'tensp':'1/16 trang',
            'soluong':0,
            'dongia':500,
            'isBuy':false
        },
        {
            'tensp':'1/32 trang',
            'soluong':0,
            'dongia':300,
            'isBuy':false
        },
        {
            'tensp':'Chân trang',
            'soluong':0,
            'dongia':100,
            'isBuy':false
        }
    ];

    $scope.tongtien=function(){
        var s=0;
        $scope.products.forEach(p =>{
        if(p.isBuy){
            s=s+p.soluong*p.dongia;
        }
        });
        return s; 
    }
    $scope.clickAll=function(){
        $scope.products.forEach(p =>{
            p.isBuy=!p.isBuy;
        })
    }
});
    app.filter('search',function(){
        return function(input, gia){
            var xuat=[];
        if(isNaN(gia)){
            xuat=input;
        }
        else{
            input.forEach(a =>{
            if(a.dongia>=gia){
                xuat.push(a);
            }
            });
        }
        return xuat;
        }
    });

